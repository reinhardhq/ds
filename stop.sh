#!/bin/bash

ps aux | grep "python index.py" | awk '{print $2}' | xargs kill -9
ps aux | grep "chromedriver" | awk '{print $2}' | xargs kill -9
