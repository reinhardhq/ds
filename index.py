# coding:utf-8

from selenium import webdriver
import os
import traceback
import time
import pandas as pd
import logging
import argparse
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.chrome.options import Options
from retry import retry

# result
TEN_GIGA = '10ギガ'
FIVE_GIGA = '5ギガ'
ONE_GIGA = '1ギガ'

# zipcode csv file
# 2018/7/2 http://jusyo.jp/csv/new.php
# 2018/8/2 kokudo_chiriin.csv
CSV_PATH = os.path.dirname(os.path.abspath(__file__)) + '/kokudo_chiriin.csv'

# selenium wait sec
WAIT_TIME = 3

# selenium timeout exception sec
EXCEPT_TIME = 4

RESULT_CSV = ''

############################################################
#
# 前処理
#
############################################################
logger = logging.getLogger(__name__)
fmt = "%(asctime)s %(levelname)s %(name)s :%(message)s"
logging.basicConfig(level=logging.INFO, filename='au_hikari.log', format=fmt)

# コマンドライン実行引数処理
parser = argparse.ArgumentParser()
parser.add_argument(
    "--debug", help="--debug is browser mode option", action="store_true")
parser.add_argument("--limit", help="--limit is define exec number")
parser.add_argument("--offset", help="--offset is define start number")
parser.add_argument("--mode", help="--mode is add or new")
parser.add_argument("--file", help="--file is output csv_file_name ")
parser.add_argument("--csv", help="--csv is input csv_file_name ")
parser.add_argument(
    "--ss", help="--ss is capture screenshot option", action="store_true")
args = parser.parse_args()

# file_nameの指定がない場合
if args.file is not None:
    RESULT_CSV = os.path.dirname(os.path.abspath(
        __file__)) + '/' + args.file
else:
    RESULT_CSV = os.path.dirname(os.path.abspath(
        __file__)) + '/result_au_hikari.csv'

# chromedriver
DRIVER_PATH = os.path.dirname(os.path.abspath(__file__)) + '/chromedriver'

if args.debug:
    driver = webdriver.Chrome(executable_path=DRIVER_PATH)
else:
    options = Options()
    options.add_argument('--headless')
    driver = webdriver.Chrome(
        executable_path=DRIVER_PATH, chrome_options=options)

# テスト用csvファイルを指定する為のオプション
if args.csv is not None:
    CSV_PATH = os.path.dirname(os.path.abspath(__file__)) + '/' + args.csv
else:
    CSV_PATH = os.path.dirname(os.path.abspath(__file__)) + '/kokudo_chiriin.csv'

# クロール対象
WEB_SITE = 'https://bb-application.au.kddi.com/auhikari/zipcode'

# select first elements
SELECT_ELEMENT = '//*[@id="selection-block"]/table/tbody/tr[1]/td[1]/a'

# 繰り返し画面遷移用
EXCEPT_SELECT_ELEMENT_CLASS_NAME = 'heightLine-areaptA'

# auひかりホームS 判定用
IS_AU_HIKARI_TYPE = '//*[@id="contents"]/div/form/div[4]/div/ul/li[2]'

# お申し込み受付中 判定用
IS_SERVICE = '//*[@id="contents"]/div/form/div[4]/div/ul/li[1]/p'

# 号・番地を入力 判定用
NEED_DETAIL = '//*[@id="contents"]/div/form/div[1]/div/p'



############################################################
#
# scrennshot取得
#
############################################################
def screenshot_page(_driver,_zipcode):

    screenshot_filename = os.path.dirname(os.path.abspath(__file__)) + '/screenshots/' + _zipcode + '.png'
    _driver.save_screenshot(screenshot_filename)



############################################################
#
# 全国郵便番号取得
#
############################################################


def load_zipcode_csv():
    zip_list = []
    zipcodes = pd.read_csv(
        CSV_PATH, usecols=[1], encoding="shift_jis")
    zipcodes_unique = zipcodes.drop_duplicates()
    print(' Csv count : {0}'.format(len(zipcodes_unique)))
    for index, row in zipcodes_unique.iterrows():
        zipcode = row.values
        zip_list.append(zipcode)
    return zip_list


############################################################
#
# csv初期化処理(Header定義)
#
############################################################
def init_csv():
    df = pd.DataFrame(columns=['zip_code', 'area_name', '1giga',
                               '5giga', '10giga', 'isHomeS', 'NotFound', 'NeededTel'])
    df.to_csv(RESULT_CSV, mode='w', header=True,
              index=False, encoding='shift_jis')


############################################################
#
# csv書き込み
#
############################################################
def add_row_tocsv(in_list):
    _zip_code = in_list[0]
    _area_name = in_list[1]
    _1giga = in_list[2]
    _5giga = in_list[3]
    _10giga = in_list[4]
    _isHomeS = in_list[5]
    _NotFound = in_list[6]
    _NeededTel = in_list[7]

    df = pd.DataFrame([[_zip_code, _area_name, _1giga, _5giga, _10giga,  _isHomeS,  _NotFound, _NeededTel]], columns=[
                      'zip_code', 'area_name', '1giga', '5giga', '10giga',  'isHomeS', 'NotFound', 'NeededTel'])
    df.to_csv(RESULT_CSV, mode='a', header=False,
              index=False, encoding='shift_jis')


############################################################
#
# auひかり対応サービス判定関数
#
############################################################
def can_delivery_service(_driver, _result_zip_code, _result_service_aria):

    result_element = WebDriverWait(_driver, EXCEPT_TIME).until(
        EC.presence_of_element_located(
            (By.XPATH, '//*[@id="contents"]/div/form/div[4]/div/p[1]'))
    )
    result_element_text = result_element.text

    time.sleep(WAIT_TIME)
    area_name_element = WebDriverWait(_driver, EXCEPT_TIME).until(
        EC.presence_of_element_located(
            (By.XPATH, '//*[@id="address-box"]/div/ul/li[1]/div/p[2]'))
    )
    result_area_name = area_name_element.text

    time.sleep(WAIT_TIME)
    is_au_hikari_type_s_element = WebDriverWait(_driver, EXCEPT_TIME).until(
        EC.presence_of_element_located(
            (By.XPATH, IS_AU_HIKARI_TYPE))
    )
    is_au_hikari_type_name = is_au_hikari_type_s_element.text

    time.sleep(WAIT_TIME)
    is_au_hikari_homes_service_element = WebDriverWait(_driver, EXCEPT_TIME).until(
        EC.presence_of_element_located(
            (By.XPATH, IS_SERVICE))
    )
    is_au_hikari_homes_service_text = is_au_hikari_homes_service_element.text

    # 結果判定
    result_giga_one = True if ONE_GIGA in result_element_text else False
    result_giga_five = True if FIVE_GIGA in result_element_text else False
    result_giga_ten = True if TEN_GIGA in result_element_text else False

    # サービス提供エリア外
    if result_giga_one is False and result_giga_five is False and result_giga_ten is False:
        # ホーム(S)提供エリア判定 (申し込み受付中判定)
        if is_au_hikari_type_name in 'auひかり ホーム(S)' and is_au_hikari_homes_service_text in 'お申し込み受付中':
            _result_service_aria = [_result_zip_code, result_area_name,
                                    result_giga_one, result_giga_five, result_giga_ten, True, False, False]

        # auひかりホーム未提供エリア
        elif is_au_hikari_type_name in 'auひかり ホーム(S)' or is_au_hikari_type_name in 'auひかりホーム':
            _result_service_aria = [_result_zip_code, result_area_name,
                                    result_giga_one, result_giga_five, result_giga_ten, False, False, False]

        # 想定外な状態
        else:
            result_area_name = 'ERR0002:想定外な状態です(サービス提供外かつauひかりホーム文字列も含まれない)'
            _result_service_aria = [_result_zip_code,
                                   result_area_name, False, False, False, False, False, False]

    # サービス提供エリア
    else:
        _result_service_aria = [_result_zip_code, result_area_name,
                                result_giga_one, result_giga_five, result_giga_ten, False, False, False]

    add_row_tocsv(_result_service_aria)

    logger.info(' ZIP Code : {0} is {1} : detail {2}'.format(
        _result_zip_code, result_element_text, is_au_hikari_type_name))


############################################################
#
# 番地・号入力関数
#
############################################################
def input_address(_driver, _result_zip_code, _result_service_aria):

    # add1
    add1 = driver.find_element_by_id('add1')
    # キャッシュにより前回入力値がdriver単位で保持されるためclearが必須
    add1.clear()
    add1.send_keys(1)

    # add2
    add2 = driver.find_element_by_id('add2')
    # キャッシュにより前回入力値がdriver単位で保持されるためclearが必須
    add2.clear()
    add2.send_keys(1)

    # search
    detail_check_button = WebDriverWait(driver, EXCEPT_TIME).until(
        EC.presence_of_element_located(
            (By.XPATH, '//*[@id="RightBtn"]/span/span'))
    )
    detail_check_button.click()

    try:
        while True:
            time.sleep(WAIT_TIME)

            element_next_page = driver.find_element_by_class_name(
                EXCEPT_SELECT_ELEMENT_CLASS_NAME)
            element_next_page.click()

    # 最終ページ到達
    except NoSuchElementException:
        try:
            # 最終ページ
            time.sleep(WAIT_TIME)
            can_delivery_service(_driver, _result_zip_code, _result_service_aria)

        # 存在しないZIPCode もしくは 電話番号入力が必要 もしくは 提供エリアではない
        except TimeoutException:
            try:
                time.sleep(WAIT_TIME)
                # need_detail_info_element = WebDriverWait(driver, EXCEPT_TIME).until(
                #     EC.presence_of_element_located(
                #         (By.XPATH, '//*[@id="contents"]/div/p[1]'))
                # )
                #
                # need_detail_info_message = need_detail_info_element.text

                area_name_element = WebDriverWait(driver, EXCEPT_TIME).until(
                    EC.presence_of_element_located(
                        (By.XPATH, '//*[@id="address-box"]/div/ul/li[1]/div/p[2]'))
                )

                result_area_name = area_name_element.text

                # if '提供エリアを詳細に確認するため' in need_detail_info_message:

                need_address_or_phone_element = WebDriverWait(driver, EXCEPT_TIME).until(
                    EC.presence_of_element_located(
                        (By.XPATH, '//*[@id="contents"]/div/form/div[1]/div/p'))
                )

                need_address_or_phone_text = need_address_or_phone_element.text
                # print(need_address_or_phone_text)
                if '電話番号' in need_address_or_phone_text:
                    _result_service_aria = [
                        _result_zip_code, result_area_name, False, False, False, False, False, True]

                    add_row_tocsv(_result_service_aria)

                    logger.info(' ZIP Code : {0} is {1}'.format(
                        _result_zip_code, result_area_name))
                #
                # else:
                #     result_area_name = 'ERR0001:想定外な状態です(input_address:電話番号が必要と判定されたが、文字列が含まれない)'
                #     _result_service_aria = [_result_zip_code,
                #                            result_area_name, False, False, False, False, False, False]
                #
                #     add_row_tocsv(_result_service_aria)

            except WebDriverException:
                try:
                    time.sleep(WAIT_TIME)
                    error_message_element = WebDriverWait(driver, EXCEPT_TIME).until(
                        EC.presence_of_element_located(
                            (By.XPATH, '//*[@id="contents"]/div/div[2]'))
                    )
                    error_message_text = error_message_element.text

                    result_area_name = '郵便番号と合致する住所が見つかりませんでした'

                    if '郵便番号と合致する住所が見つかりませんでした' in error_message_text:
                        _result_service_aria = [_result_zip_code,
                                               result_area_name, False, False, False, False, True, False]

                    add_row_tocsv(_result_service_aria)

                    logger.info(' ZIP Code : {0} is {1}'.format(
                        _result_zip_code, result_area_name))

                except NoSuchElementException:
                    result_area_name = 'ERR0003:想定外な状態です(合致住所が見つからない：NoSuchElementException)'
                    _result_service_aria = [_result_zip_code,
                                           result_area_name, False, False, False, False, False, False]

                    add_row_tocsv(_result_service_aria)

                except TimeoutException:
                    result_area_name = 'ERR0004:想定外な状態です(合致住所が見つからない：TimeoutException)'
                    _result_service_aria = [_result_zip_code,
                                           result_area_name, False, False, False, False, False, False]

                    add_row_tocsv(_result_service_aria)

############################################################
#
# auひかり自動入力関数
#
############################################################
@retry(delay=10, backoff=2, max_delay=80)
def au_hikari(in_zip1, in_zip2):

    driver.get(WEB_SITE)

    result_service_aria = []

    result_zip_code = str(in_zip1) + '-' + str(in_zip2)

    # 1page
    # click home type
    driver.find_element_by_xpath('//*[@id="home"]').click()

    # zip1
    zip1 = driver.find_element_by_id('sendzip1')
    # キャッシュにより前回入力値がdriver単位で保持されるためclearが必須
    zip1.clear()
    zip1.send_keys(in_zip1)

    # zip2
    zip2 = driver.find_element_by_id('sendzip2')
    # キャッシュにより前回入力値がdriver単位で保持されるためclearが必須
    zip2.clear()
    zip2.send_keys(in_zip2)

    # search
    search_button = WebDriverWait(driver, EXCEPT_TIME).until(
        EC.presence_of_element_located(
            (By.XPATH, '//*[@id="BtnZip"]/span/a/span/span'))
    )
    search_button.click()

    # NoSuchElementException / TimeoutException捕捉のためのtry
    try:
        while True:
            time.sleep(WAIT_TIME)

            element_next_page = driver.find_element_by_class_name(
                EXCEPT_SELECT_ELEMENT_CLASS_NAME)
            element_next_page.click()

    # 最終ページ到達
    except NoSuchElementException:

        try:
            # 最終ページ
            time.sleep(WAIT_TIME)
            can_delivery_service(driver, result_zip_code, result_service_aria)

        # 存在しないZIPCode もしくは 電話番号入力が必要 もしくは 提供エリアではない
        except TimeoutException:
            try:
                time.sleep(WAIT_TIME)
                need_detail_info_element = WebDriverWait(driver, EXCEPT_TIME).until(
                    EC.presence_of_element_located(
                        (By.XPATH, '//*[@id="contents"]/div/p[1]'))
                )

                need_detail_info_message = need_detail_info_element.text

                area_name_element = WebDriverWait(driver, EXCEPT_TIME).until(
                    EC.presence_of_element_located(
                        (By.XPATH, '//*[@id="address-box"]/div/ul/li[1]/div/p[2]'))
                )

                result_area_name = area_name_element.text

                if '提供エリアを詳細に確認するため' in need_detail_info_message:

                    need_address_or_phone_element = WebDriverWait(driver, EXCEPT_TIME).until(
                        EC.presence_of_element_located(
                            (By.XPATH, '//*[@id="contents"]/div/form/div[1]/div/p'))
                    )

                    need_address_or_phone_text = need_address_or_phone_element.text

                    if '番地' in need_address_or_phone_text:
                        input_address(driver,result_zip_code,result_service_aria)


                    if '電話番号' in need_address_or_phone_text:

                        result_service_aria = [
                            result_zip_code, result_area_name, False, False, False, False, False, True]

                        add_row_tocsv(result_service_aria)

                else:
                    result_area_name = 'エリア名が取得出来ません。'
                    """result_area_name = 'ERR0001:想定外な状態です(電話番号が必要と判定されたが、文字列が含まれない)'"""
                    result_service_aria = [result_zip_code,
                                           result_area_name, False, False, False, False, True, False]

                    add_row_tocsv(result_service_aria)

            except WebDriverException:
                try:
                    time.sleep(WAIT_TIME)
                    error_message_element = WebDriverWait(driver, EXCEPT_TIME).until(
                        EC.presence_of_element_located(
                            (By.XPATH, '//*[@id="contents"]/div/div[2]'))
                    )
                    error_message_text = error_message_element.text

                    result_area_name = '郵便番号と合致する住所が見つかりませんでした'

                    if '郵便番号と合致する住所が見つかりませんでした' in error_message_text:
                        result_service_aria = [result_zip_code,
                                               result_area_name, False, False, False, False, True, False]

                    add_row_tocsv(result_service_aria)

                    logger.info(' ZIP Code : {0} is {1}'.format(
                        result_zip_code, result_area_name))

                except NoSuchElementException:
                    result_area_name = 'ERR0003:想定外な状態です(合致住所が見つからない：NoSuchElementException)'
                    result_service_aria = [result_zip_code,
                                           result_area_name, False, False, False, False, False, False]

                    add_row_tocsv(result_service_aria)

                except TimeoutException:
                    result_area_name = 'ERR0004:想定外な状態です(合致住所が見つからない：TimeoutException)'
                    result_service_aria = [result_zip_code,
                                           result_area_name, False, False, False, False, False, False]

                    add_row_tocsv(result_service_aria)

    finally:
        if args.ss:
            screenshot_page(driver, result_zip_code)
        else:
            pass


############################################################
#
# au_hikari_list
#
############################################################
def au_hikari_list(list):
    for zip_code in list:
        str_zip_code = str(zip_code[0])
        # split zip code xxxyyyy -> xxx,yyyy
        s_zip1 = str_zip_code[:3]
        s_zip2 = str_zip_code[3:]
        au_hikari(s_zip1, s_zip2)


############################################################
#
# main
#
############################################################
if __name__ == '__main__':
    try:

        list_zip = load_zipcode_csv()

        # limit
        _limit = int(args.limit) if args.limit else None

        if args.mode == 'new':
            init_csv()

        # offset
        if args.offset:
            # listの開始はゼロ開始のため減算する
            offset = args.offset
            _offset = int(offset)

            if _limit is None:
                _list_zip = list_zip[(_offset-1):]
            else:
                _list_zip = list_zip[(_offset-1):(_limit)]
        else:
            if _limit is None:
                _list_zip = list_zip
            else:
                _list_zip = list_zip[:(_limit)]

        logger.info('[Main] offset={0} ,list_size={1} ,limit={2}'.format((args.offset), len(_list_zip), (_limit)))

        au_hikari_list(_list_zip)

    # TimeoutException / NoSuchElementException以外を捕捉
    except:
        print(traceback.format_exc())
        logger.error(traceback.format_exc())

    finally:
        driver.close()
